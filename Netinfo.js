import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import NetInfo from '@react-native-community/netinfo';

const Netinfo = () => {
  const netinfo = NetInfo.useNetInfo();
  console.log(netinfo.details);

  return (
    <View style={style.View}>
      <View style={style.View2}>
        <View style={style.View3}>
          <Text>Type</Text>
          <Text>:</Text>
        </View>
        <View style={{}}>
          <Text>{netinfo.type}</Text>
        </View>
      </View>

      <Text>Is Connected : {netinfo.isConnected?.toString()}</Text>
      <Text>Is Wifi Enabled : {netinfo.isWifiEnabled?.toString()}</Text>
      <Text>
        Is Internet Reachable : {netinfo.isInternetReachable?.toString()}
      </Text>
      <Text>Frequency : {netinfo.details?.frequency}</Text>
      <Text>Ip Address : {netinfo.details?.ipAddress}</Text>
      <Text>Strength : {netinfo.details?.strength}</Text>
      <Text>LinkSpeed : {netinfo.details?.linkSpeed}</Text>
      <Text>Subnet : {netinfo.details?.subnet}</Text>
    </View>
  );
};

const style = StyleSheet.create({
  View: {
    flex: 1,
    backGroundColor: 'lightgray',
    paddingTop: 80,
    paddingHorizontal: 50,
    alignItems: 'baseline',
  },
  View2: {
    width: '100%',
    flexDirection: 'row',
  },
  View3: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  View4: {},
});
export default Netinfo;
