import React, {useState} from 'react';
import {View, Text} from 'react-native';
import Slider from '@react-native-community/slider';

export default function Sliding() {
  const [range, setRange] = useState('50%');
  const [sliding, setSliding] = useState('non-active');

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'lightgray',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Text style={{fontSize: 18, fontWeight: 'bold'}}>{range}</Text>
      <Text style={{fontSize: 18, fontWeight: 'bold'}}>{sliding}</Text>
      <Slider
        style={{width: 200, height: 40}}
        minimumValue={0}
        maximumValue={1}
        minimumTrackTintColor="#F7F7F7"
        maximumTrackTintColor="#000000"
        thumbTintColor="blue"
        value={0.5}
        onValueChange={value => setRange(parseInt(value * 100) + '%')}
        onSlidingStart={() => setSliding('sliding')}
        onSlidingComplete={() => setSliding('non-active')}
      />
    </View>
  );
}
