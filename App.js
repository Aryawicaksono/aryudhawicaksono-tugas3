import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Netinfo from './Netinfo';
import Sliding from './Sliding';
import DatePicker from './datepicker';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

function Navigator({navigation}) {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'F7F7F7',
        paddingTop: 50,
        alignItems: 'center',
      }}>
      <TouchableOpacity
        onPress={() => navigation.navigate('Netinfo')}
        style={{
          width: '60%',
          height: 50,
          backgroundColor: 'blue',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontSize: 18,
            fontWeight: 'bold',
            color: 'white',
          }}>
          Netinfo
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate('DatePicker')}
        style={{
          marginTop: 20,
          width: '60%',
          height: 50,
          backgroundColor: 'blue',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontSize: 18,
            fontWeight: 'bold',
            color: 'white',
          }}>
          Date Time Picker
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => navigation.navigate('Sliding')}
        style={{
          marginTop: 20,
          width: '60%',
          height: 50,
          backgroundColor: 'blue',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text
          style={{
            fontSize: 18,
            fontWeight: 'bold',
            color: 'white',
          }}>
          Slider
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName="Navigator">
        <Stack.Screen name="Navigator" component={Navigator} />
        <Stack.Screen name="Netinfo" component={Netinfo} />
        <Stack.Screen name="DatePicker" component={DatePicker} />
        <Stack.Screen name="Sliding" component={Sliding} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
